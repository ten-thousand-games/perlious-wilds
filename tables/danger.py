DANGERS = {
    'Danger': [
        '[Unnatural Entity]',
        '[Hazard]',
        '[Hazard]',
        '[Hazard]',
        '[Hazard]',
        '[Hazard]',
        '[Creature]',
        '[Creature]',
        '[Creature]',
        '[Creature]',
        '[Creature]',
        '[Creature]'
    ],

    'Unnatural Entity': [
        '[Undead Entity]',
        '[Undead Entity]',
        '[Undead Entity]',
        '[Undead Entity]',
        '[Undead Entity]',
        '[Undead Entity]',
        '[Undead Entity]',
        '[Undead Entity]',
        '[Planar Entity]',
        '[Planar Entity]',
        '[Planar Entity]',
        '[Divine Entity]'
    ],
    'Hazard': [
        '[Unnatural Hazard]',
        '[Unnatural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Natural Hazard]',
        '[Trap]',
        '[Trap]'
    ],
    'Undead Entity': [
        'haunt/wisp [Undead Entity Tags]',
        'haunt/wisp [Undead Entity Tags]',
        'haunt/wisp [Undead Entity Tags]',
        'haunt/wisp [Undead Entity Tags]',
        'ghost/spectre [Undead Entity Tags]',
        'ghost/spectre [Undead Entity Tags]',
        'ghost/spectre [Undead Entity Tags]',
        'ghost/spectre [Undead Entity Tags]',
        'banshee [Undead Entity Tags]',
        'wraith/wight [Undead Entity Tags]',
        'wraith/wight [Undead Entity Tags]',
        'spirit lord/master [Undead Entity Tags]'
    ],
    'Undead Entity Tags': [
        '[Ability], [Activity], [Alignment], [Disposition]',
    ],
    'Planar Entity': [
        'imp, small [Planar Entity Tags]',
        'imp, small [Planar Entity Tags]',
        'imp, small [Planar Entity Tags]',
	'lesser elemental [Planar Entity Tags]',
	'lesser elemental [Planar Entity Tags]',
	'lesser elemental [Planar Entity Tags]',
	'lesser demon/horror [Planar Entity Tags]',
	'lesser demon/horror [Planar Entity Tags]',
	'lesser demon/horror [Planar Entity Tags]',
	'greater elemental [Planar Entity Tags]',
	'greater demon/horror [Planar Entity Tags]',
	'devil/elemental lord [Planar Entity Tags]'
    ],
    'Planar Entity Tags': [
        '[Ability], [Activity], [Alignment], [Disposition], [Element], [Feature], [Tag]'
    ],
    'Divine Entity': [
        'agent [Divine Entity Tags]',
        'agent [Divine Entity Tags]',
        'agent [Divine Entity Tags]',
        'agent [Divine Entity Tags]',
        'agent [Divine Entity Tags]',
        'champion [Divine Entity Tags]',
        'champion [Divine Entity Tags]',
        'champion [Divine Entity Tags]',
        'champion [Divine Entity Tags]',
        'army (horde) [Divine Entity Tags]',
        'army (horde) [Divine Entity Tags]',
        'avatar [Divine Entity Tags]'
    ],
    'Divine Entity Tags': [
        '[Ability], [Activity], [Alignment], [Aspect], [Disposition], [Element], [Feature], [Tag]',
    ],
    'Unnatural Hazard': [
        'taint/blight/curse [Unnatural Hazard Tags]',
        'taint/blight/curse [Unnatural Hazard Tags]',
        'taint/blight/curse [Unnatural Hazard Tags]',
        'arcane trap/effect [Unnatural Hazard Tags]',
        'arcane trap/effect [Unnatural Hazard Tags]',
        'arcane trap/effect [Unnatural Hazard Tags]',
        'arcane trap/effect [Unnatural Hazard Tags]',
        'arcane trap/effect [Unnatural Hazard Tags]',
        'planar trap/effect [Unnatural Hazard Tags]',
        'planar trap/effect [Unnatural Hazard Tags]',
        'planar trap/effect [Unnatural Hazard Tags]',
        'divine [Unnatural Hazard Tags]'
    ],
    'Unnatural Hazard Tags': [
        '[Aspect], [Visibility]'
    ],
    'Natural Hazard': [
        'blinding mist/fog',
        'blinding mist/fog',
        'bog/mire/quicksand',
        'bog/mire/quicksand',
        'pitfall/sinkhole/chasm',
        'pitfall/sinkhole/chasm',
        'pitfall/sinkhole/chasm',
        'poison/disease',
        'poison/disease',
        'flood/fire/tornado',
        'flood/fire/tornado',
        '[Oddity]'
    ],
    'Trap': [
        'alarm [Trap Tags]',
        'alarm [Trap Tags]',
        'ensnaring/paralyzing [Trap Tags]',
        'ensnaring/paralyzing [Trap Tags]',
        'ensnaring/paralyzing [Trap Tags]',
        'injurious (pit, etc.) [Trap Tags]',
        'injurious (pit, etc.) [Trap Tags]',
        'injurious (pit, etc.) [Trap Tags]',
        'gas/fire/poison [Trap Tags]',
        'ambush [Trap Tags]',
        'ambush [Trap Tags]',
        'ambush [Trap Tags]'
    ],
    'Trap Tags': [
        'from [Creature], [Aspect], [Visibility]'
    ],
}
