DISCOVERIES = {
    'Discovery': [
        '[Unnatural Feature]',
        '[Natural Feature]',
        '[Natural Feature]',
        '[Natural Feature]',
        '[Evidence]',
        '[Evidence]',
        '[Creature]',
        '[Creature]',
        '[Structure] (Built By [Creature=1d4+4])',
        '[Structure] (Built By [Creature=1d4+4])',
        '[Structure] (Built By [Creature=1d4+4])',
        '[Structure] (Built By [Creature=1d4+4])',
    ],

    'Unnatural Feature': [
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Arcane Feature] ([Alignment], [Magic Type])',
        '[Planar Feature] ([Alignment], [Element])',
        '[Planar Feature] ([Alignment], [Element])',
        '[Divine Feature] ([Alignment], [Aspect])'
    ],
    'Natural Feature': [
        '[Lair] ([Creature], [Visibility])',
        '[Lair] ([Creature], [Visibility])',
        '[Obstacle]',
        '[Obstacle]',
        '[Terrain Change]',
        '[Terrain Change]',
        '[Terrain Change]',
        '[Water Feature]',
        '[Water Feature]',
        '[Landmark]',
        '[Landmark]',
        '[Resource] ([Size], [Visibility])'
    ],
    'Evidence': [
        '[Tracks/Spoor] ([Age], [Creature])',
        '[Tracks/Spoor] ([Age], [Creature])',
        '[Tracks/Spoor] ([Age], [Creature])',
        '[Tracks/Spoor] ([Age], [Creature])',
        '[Tracks/Spoor] ([Age], [Creature])',
        '[Tracks/Spoor] ([Age], [Creature])',
        '[Remains/Debris] ([Age], [Visibility])',
        '[Remains/Debris] ([Age], [Visibility])',
        '[Remains/Debris] ([Age], [Visibility])',
        '[Remains/Debris] ([Age], [Visibility])',
        '[Stash/Cache]',
        '[Stash/Cache]'
    ],
    'Structure': [
        '[Enigmatic Structure] ([Age=1d8+4], [Size=1d8+4], [Visibility])',
        '[Infrastructure]',
        '[Infrastructure]',
        '[Dwelling]',
        '[Burial/Religious Structure] ([Alignment], [Aspect])',
        '[Burial/Religious Structure] ([Alignment], [Aspect])',
        '[Steading]',
        '[Steading]',
        '[Ruin] ([Age=1d8+4], [Ruination], [Visibility])',
        '[Ruin] ([Age=1d8+4], [Ruination], [Visibility])',
        '[Ruin] ([Age=1d8+4], [Ruination], [Visibility])',
        '[Ruin] ([Age=1d8+4], [Ruination], [Visibility])'
    ],

    'Arcane Feature': [
        'residue',
        'residue',
        'blight',
        'blight',
        'blight',
        'alteration/mutation',
        'alteration/mutation',
        'enchantment',
        'enchantment',
        'enchantment',
        'source/repository',
        'source/repository'
    ],
    'Planar Feature': [
        'distortion/warp',
        'distortion/warp',
        'distortion/warp',
        'distortion/warp',
        'portal/gate',
        'portal/gate',
        'portal/gate',
        'portal/gate',
        'rift/tear',
        'rift/tear',
        'outpost'
        'outpost'
    ],
    'Divine Feature': [
        'mark/sign',
        'mark/sign',
        'mark/sign',
        'cursed place',
        'cursed place',
        'cursed place',
        'hallowed place',
        'hallowed place',
        'hallowed place',
        'watched place',
        'watched place',
        'presence'
    ],
    'Lair': [
        'burrow',
        'burrow',
        'burrow',
        'cave/tunnels',
        'cave/tunnels',
        'cave/tunnels',
        'cave/tunnels',
        'nest/aerie',
        'nest/aerie',
        'hive',
        'ruins ([Structure])',
        'ruins ([Structure])'
    ],
    'Obstacle': [
        'difficult ground',
        'difficult ground',
        'difficult ground',
        'difficult ground',
        'difficult ground',
        'cliff/crevasse/chasm',
        'cliff/crevasse/chasm',
        'cliff/crevasse/chasm',
        'ravine/gorge',
        'ravine/gorge',
        '[Oddity]',
        '[Oddity]'
    ],
    'Terrain Change': [
        'limited area of another terrain',
        'limited area of another terrain',
        'limited area of another terrain',
        'limited area of another terrain',
        'crevice/hole/pit/cave',
        'crevice/hole/pit/cave',
        'altitude change',
        'altitude change',
        'canyon/valley',
        'canyon/valley',
        'rise/peak in distance',
        'rise/peak in distance'
    ],
    'Water Feature': [
        'spring/hotspring',
        'waterfall/geyser',
        'creek/stream/brook',
        'creek/stream/brook',
        'creek/stream/brook',
        'creek/stream/brook',
        'pond/lake',
        'pond/lake',
        'river',
        'river',
        'sea/ocean',
        'sea/ocean'
    ],
    'Landmark': [
        'water-based (waterfall, geyser, etc)',
        'water-based (waterfall, geyser, etc)',
        'water-based (waterfall, geyser, etc)',
        'plant-based (ancient tree, giant flowers, etc)',
        'plant-based (ancient tree, giant flowers, etc)',
        'plant-based (ancient tree, giant flowers, etc)',
        'earth-based (peak, formation, crater, etc)',
        'earth-based (peak, formation, crater, etc)',
        'earth-based (peak, formation, crater, etc)',
        'earth-based (peak, formation, crater, etc)',
        '[Oddity]',
        '[Oddity]'
    ],
    'Resource': [
        'game/fruit/vegetable',
        'game/fruit/vegetable',
        'game/fruit/vegetable',
        'game/fruit/vegetable',
        'herb/spice/dye source',
        'herb/spice/dye source',
        'timber/stone',
        'timber/stone',
        'timber/stone',
        'ore',
        'ore',
        'precious metal/gems'
    ],
    'Tracks/Spoor': [
        'faint/unclear',
        'faint/unclear',
        'faint/unclear',
        'definite/clear',
        'definite/clear',
        'definite/clear',
        'multiple',
        'multiple',
        'signs of violence',
        'signs of violence',
        'trail of blood/other',
        'trail of blood/other'
    ],
    'Remains/Debris': [
        'bones',
        'bones',
        'bones',
        'bones',
        'corpse/carcass',
        'corpse/carcass',
        'corpse/carcass',
        'site of violence',
        'site of violence',
        'junk/refuse',
        'lost supplies/cargo',
        'tools/weapons/armor'
    ],
    'Stash/Cache': [
        'trinkets/coins',
        'trinkets/coins',
        'trinkets/coins',
        'tools/weapons/armor',
        'tools/weapons/armor',
        'map',
        'map',
        'food/supplies',
        'food/supplies',
        '[Treasure]',
        '[Treasure]',
        '[Treasure]'
    ],
    'Enigmatic Structure': [
	'earthworks',
	'earthworks',
	'earthworks',
	'earthworks',
	'megalith',
	'megalith',
	'megalith',
	'megalith',
	'statue/idol/totem',
	'statue/idol/totem',
	'statue/idol/totem',
	'[Oddity] structure'
    ],
    'Infrastructure': [
	'track/path',
	'track/path',
	'track/path',
	'track/path',
	'road',
	'road',
	'road',
	'road',
	'bridge/ford',
	'bridge/ford',
	'mine/quarry',
	'aqueduct/canal/portal'
    ],
    'Dwelling': [
	'campsite',
	'campsite',
	'campsite',
	'hovel/hut',
	'hovel/hut',
	'hovel/hut',
	'farm',
	'farm',
	'inn/roadhouse',
	'inn/roadhouse',
	'tower/keep/estate',
	'tower/keep/estate'
    ],
    'Burial/Religious Structure': [
	'grave marker/barrow',
	'grave marker/barrow',
	'graveyard/necropolis',
	'graveyard/necropolis',
	'tomb/crypt',
	'tomb/crypt',
	'shrine',
	'shrine',
	'shrine',
	'temple/retreat',
	'temple/retreat',
	'great temple'
    ],
    'Ruin': [
	'[Infrastructure=1d6+6]',
	'[Infrastructure=1d6+6]',
	'[Dwelling=1d8+4]',
	'[Dwelling=1d8+4]',
	'[Burial/Religious=1d8+4]',
	'[Burial/Religious=1d8+4]',
	'[Steading=1d10+2]',
	'[Steading=1d10+2]',
	'[Dungeon]',
	'[Dungeon]',
	'[Dungeon]',
	'[Dungeon]'
    ]
}
