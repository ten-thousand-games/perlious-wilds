import re
import random

from .creature import CREATURES
from .danger import DANGERS
from .detail import DETAILS
from .discovery import DISCOVERIES
from .dungeons import DUNGEONS
from .name import NAMES
from .npc import NPCS
from .steading import STEADINGS


ALL_TABLES = {
    **CREATURES,
    **DANGERS,
    **DETAILS,
    **DISCOVERIES,
    **DUNGEONS,
    **NAMES,
    **NPCS,
    **STEADINGS,
}


SUBTABLE_RE = re.compile('\[([^\]]*)\]')


def table_roll(table, diestr=None):
    if not table and not diestr:
        return None

    minus = plus = None

    if diestr:
        if '-' in diestr:
            diestr, minus = diestr.split('-')
        elif '+' in diestr:
            diestr, plus = diestr.split('+')
        num, sides = diestr.split('d')
        num = int(num)
        sides = int(sides)
    else:
        num = 1
        sides = len(table)

    rollval = sum(random.randrange(sides) for die in range(num))

    if minus:
        rollval -= int(minus)
    elif plus:
        rollval += int(plus)

    if rollval < 0: rollval = 0
    if table:
        if rollval >= len(table): rollval = len(table)
        return table[rollval]
    else:
        return str(rollval)


def process_text(text, depth=0):
    """
    Replace subtable strings with results from another table
    """
    subtables = SUBTABLE_RE.findall(text)

    for tablestr in subtables:
        roll = None
        if '=' in tablestr:
            table, roll = tablestr.split('=')
        else:
            table = tablestr

        tblval = table_roll(ALL_TABLES.get(table), roll)
        if tblval:
            val = process_text(tblval, depth+1)

            if table:
                prefix = '\n{}{}: '.format('  '*depth, table)
            else:
                prefix = ''

            text = text.replace('[{}]'.format(tablestr), '{}{}'.format(prefix, str(val)), 1)

    return text
