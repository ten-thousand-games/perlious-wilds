import sys

from tables.generate import generate


if __name__ == '__main__':

    for what in sys.argv[1:]:
        print(generate(what))
        print()
